import React from "react";
import "./SearchPanel.css";
import { Form } from "react-bootstrap";

function SearchPanel({ searchFilter, search }) {
  return (
    <div className="search-panel">
      <Form.Control
        placeholder="type to search"
        value={search}
        onChange={searchFilter}
      />
    </div>
  );
}
export default SearchPanel