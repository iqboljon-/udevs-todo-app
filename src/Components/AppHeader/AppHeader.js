import './AppHeader.css'

function AppHeader (todo, done) {
    return (
        <div className="pt-5">
            <div className="d-flex align-items-center justify-content-between">
                <h1 className="">Todo List</h1>
                <p>{done} more to do, {todo} done</p>
            </div>
        
        </div>
    )
}

export default AppHeader
    